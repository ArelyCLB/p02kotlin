package com.example.practica02imc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.practicak02imc.R

class MainActivity : AppCompatActivity() {
    private lateinit var lblPeso: EditText
    private lateinit var lblAltura: EditText
    private lateinit var txtRes: TextView
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lblPeso = findViewById(R.id.lblPeso)
        lblAltura = findViewById(R.id.lblAltura)
        txtRes = findViewById(R.id.txtRes)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)

        btnCalcular.setOnClickListener {
            calcularIMC()
        }
        btnLimpiar.setOnClickListener {
            lblPeso.setText("")
            lblAltura.setText("")
            txtRes.setText("")
        }
        btnCerrar.setOnClickListener {
            finish()
        }
    }

    private fun calcularIMC() {
        val txtPeso = lblPeso.text.toString()
        val txtAltura = lblAltura.text.toString()

        if (txtPeso.isEmpty() || txtAltura.isEmpty()) {
            txtRes.text = "Ingresa el peso y altura: "
            return
        }

        val peso = txtPeso.toFloat()
        val altura = txtAltura.toFloat()

        val imc = peso / (altura * altura)

        var txtResu = "Su IMC es: $imc"

        if (imc < 18.5) {
            txtResu += "\nBajo peso"
        } else if (imc < 25) {
            txtResu += "\nPeso normal"
        } else if (imc < 30) {
            txtResu += "\nSobrepeso"
        } else {
            txtResu += "\nObesidad"
        }

        txtRes.text = txtResu
    }
}
